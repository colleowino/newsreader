package com.example.newsreader;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.util.Log;
import android.content.Intent;

public class Dashboard extends AppCompatActivity {
    private static final String TAG = "XHAX";

    String[] categoryLabels = {"National News","Counties", "business", "Sports", "Entertainment"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.dashboard_newslist, categoryLabels);

        ListView categories = (ListView) findViewById(R.id.categories);
        categories.setAdapter(adapter);
        categories.setOnItemClickListener(new CategoryClickHandler());

    }

    public class CategoryClickHandler implements AdapterView.OnItemClickListener{
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG, "position is: "+position);
            Intent intent = new Intent(Dashboard.this, NewsViewActivity.class);
            intent.putExtra("id",Integer.toString(position+1)); //api IDs starts at 1
            intent.putExtra("category",categoryLabels[position]);
            startActivity(intent);
        }
    }
}
