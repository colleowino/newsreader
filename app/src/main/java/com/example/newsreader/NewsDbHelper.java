package com.example.newsreader;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import android.text.Html;

public class NewsDbHelper extends SQLiteOpenHelper {

private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "newsManager";

    // NewsItems table name
    private static final String TABLE_NEWS = "news";

    // NewsItems Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TYPE = "news_type";
    private static final String KEY_TITLE = "title";
    private static final String KEY_IMG = "img_url";
    private static final String KEY_CONTENT = "content";

    public NewsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_NEWS_TABLE = "CREATE TABLE " + TABLE_NEWS + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_TYPE + " INTEGER ,"
                + KEY_TITLE + " VARCHAR(200),"
                + KEY_IMG + " TEXT,"
                + KEY_CONTENT + " TEXT" + ")";
        db.execSQL(CREATE_NEWS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NEWS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new newsItem
    void addNewsItem(NewsItem newsItem) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, newsItem.getId());
        values.put(KEY_TYPE, newsItem.getNewsType()); 
        values.put(KEY_TITLE, newsItem.getTitle());
        values.put(KEY_IMG, newsItem.getImage()); 
        values.put(KEY_CONTENT, newsItem.getContent());
        // Inserting Row
        db.insert(TABLE_NEWS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single newsItem
    NewsItem getNewsItem(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NEWS, new String[] { KEY_ID,
                        KEY_TYPE, KEY_TITLE, KEY_IMG, KEY_CONTENT }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        NewsItem newsItem = new NewsItem(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getString(4));
        // return newsItem
        return newsItem;
    }

    // checking if news already saved 
    public boolean newsItemExists(int id){
        boolean value = false;
        SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = {"id"};
        String[] selectionArgs = {String.valueOf(id)};

        Cursor query = db.query(TABLE_NEWS, columns, "id = ?", selectionArgs, null, null, null);
        if(query != null && query.getCount() > 0)
            value = true;

        query.close();
        db.close();

        return value;
    }

    // Getting All NewsItems
    public List<NewsItem> getAllNewsItems(int categoryId) {
        List<NewsItem> newsItemList = new ArrayList<NewsItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NEWS
                + " WHERE "+ KEY_TYPE +" = "+ categoryId;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                NewsItem newsItem = new NewsItem();
                newsItem.setId(Integer.parseInt(cursor.getString(0)));
                newsItem.setNewsType(cursor.getString(1));
                newsItem.setTitle(cursor.getString(2));
                newsItem.setImage(cursor.getString(3));
                newsItem.setContent(cursor.getString(4));
                // Adding newsItem to list
                newsItemList.add(newsItem);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return newsItem list
        return newsItemList;
    }

    // Updating single newsItem
    public int updateNewsItem(NewsItem newsItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TYPE, newsItem.getNewsType());
        values.put(KEY_TITLE, newsItem.getTitle());
        values.put(KEY_IMG, newsItem.getImage());
        values.put(KEY_CONTENT, newsItem.getContent());

        // updating row
        return db.update(TABLE_NEWS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(newsItem.getId()) });
    }

    // Deleting single newsItem
    public void deleteNewsItem(NewsItem newsItem) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NEWS, KEY_ID + " = ?",
                new String[] { String.valueOf(newsItem.getId()) });
        db.close();
    }


    // Getting newsItems Count
    public int getNewsItemsCount(int newsType) {
        String countQuery = "SELECT  * FROM " + TABLE_NEWS 
            +" WHERE "+ KEY_TYPE +" = "+newsType;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();

        // return count
        return count;
    }

}