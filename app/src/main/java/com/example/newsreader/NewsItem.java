package com.example.newsreader;

public class NewsItem {

    int id;
    String newsType;
    String title;
    String content;
    String image;

    public NewsItem(int id, String newsType, String title, String content, String image) {
        this.id = id;
        this.newsType = newsType;
        this.title = title;
        this.content = content;
        this.image = image;
    }

    public NewsItem(){
        // empty constructor
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }

    public String getContent() {
        return content;
    }


    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;

    }

    public String getImage() {

        return image;

    }

    public void setImage(String image) {

        this.image = image;

    }

}