package com.example.newsreader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.net.ConnectivityManager;
import android.content.Context;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.text.Html;

public class NewsViewActivity extends AppCompatActivity {
    private static final String TAG = "XHAX";
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private NewsViewAdapter mGridAdapter;
    private ArrayList<NewsItem> mGridData;
    private NewsDbHelper db;
    private String FEED_URL = "http://testing.mlab-training.devs.mobi/php_news_api/news.php?news_type_id=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsview);

        String id = getIntent().getStringExtra("id");
        String category = getIntent().getStringExtra("category");
        setTitle(category);
        String complete_url = FEED_URL+id;
        db = new NewsDbHelper(this);

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        boolean isConnected;

        ConnectivityManager conectivtyManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);  
        if (conectivtyManager.getActiveNetworkInfo() != null  
            && conectivtyManager.getActiveNetworkInfo().isAvailable()  
            && conectivtyManager.getActiveNetworkInfo().isConnected()) {  
            isConnected = true;  
        } else {  
            isConnected= false;  
            Toast.makeText(NewsViewActivity.this, "Reading local data", Toast.LENGTH_SHORT).show();
        }  

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new NewsViewAdapter(this, R.layout.news_item, mGridData);
        mGridView.setAdapter(mGridAdapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                NewsItem item = (NewsItem) parent.getItemAtPosition(position);

                //Pass the image title and url to DetailsActivity
                Intent intent = new Intent(NewsViewActivity.this, NewsDetailActivity.class);

                intent.putExtra("title", item.getTitle());
                intent.putExtra("image", item.getImage());

                //Start details activity
                startActivity(intent);
            }
        });

        mProgressBar.setVisibility(View.VISIBLE);

        if(db.getNewsItemsCount(Integer.parseInt(id)) > 0){
            mGridData.addAll(new ArrayList(db.getAllNewsItems(Integer.parseInt(id))));
            mGridAdapter.notifyDataSetChanged();
            Log.e(TAG, "data: "+mGridData.size());
            mProgressBar.setVisibility(View.GONE);
        }else{
            new AsyncHttpTask().execute(complete_url);
        }

        //Start download
    }

    //Downloading data asynchronously
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    // Reading all contacts
//                    Log.d("Reading: ", "Reading all contacts..");
//                    Log.d(TAG, "got?: "+db.getNewsItemsCount(5));

                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);
            } else {
                Toast.makeText(NewsViewActivity.this, "Failed to fetch data!", Toast.LENGTH_SHORT).show();
            }
            mProgressBar.setVisibility(View.GONE);
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("news");
            NewsItem item;
            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                item = new NewsItem();

                int webID = post.getInt("id");
                item.setId(webID);
                item.setNewsType(post.getString("news_type"));
                String title = post.optString("title");
                item.setTitle(title);
                item.setImage(post.getString("image_url"));
                item.setContent(post.getString("content"));

                if(!db.newsItemExists(webID)){
                    Log.e(TAG, "creating record: "+webID);
                    db.addNewsItem(item);
                }

                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}