package com.example.newsreader;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import android.util.Log;

import com.squareup.picasso.Picasso;

public class NewsViewAdapter extends ArrayAdapter<NewsItem> {
    private String TAG = "XHAX";
    private Context mContext;

    private int layoutResourceId;
    private ArrayList<NewsItem> mGridData = new ArrayList<NewsItem>();

    public NewsViewAdapter(Context mContext, int layoutResourceId, ArrayList<NewsItem> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }

    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<NewsItem> mGridData) {
        this.mGridData = mGridData;
        Log.e("XHAX", " stuff is going round");
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.titleTextView = (TextView) row.findViewById(R.id.grid_item_title);
            holder.imageView = (ImageView) row.findViewById(R.id.grid_item_image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        NewsItem item = mGridData.get(position);
        holder.titleTextView.setText(Html.fromHtml(item.getTitle()));

        if(item.getImage().equals("null")){
            holder.imageView.setImageResource(R.drawable.ic_broken_image_black_24dp);
        }
        else{
            Picasso.with(mContext).load(item.getImage()).into(holder.imageView);
        }
        return row;
    }

    static class ViewHolder {
        TextView titleTextView;
        ImageView imageView;
    }
}
